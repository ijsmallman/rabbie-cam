# rabbie-cam

CCTV app for remote holiday cottage monitoring.

## Features:

* [x] Scheduled capture of still images
* [x] Scheduled image upload to Google Photos
* [x] Adjustable schedule settings (through environment variables)
* [x] Adjustable scheduled capture settings (through environment variables)
* [x] Automatic night exposure for captures scheduled after sunset 
(requires (lat, lon) environment variables)
* [x] Instantaneous capture of still images through REST API (with adjustable 
camera settings)
* [x] Live video (MJPEG) stream through web API 
(with adjustable camera settings)
* [x] Swagger documentation of REST API

## Environment Variables

### Required

* `SECRETS_PATH`: Directory containing secret files (e.g. credentials)
* `CREDENTIALS_PATH`: Path to Google credentials.json 
(usually in `SECRETS_PATH`)
* `IMAGES_PATH`: Directory to store captured images
* `USER_NAME`: Username for uploading images to Google Photos
* `BALENA_DEVICE_NAME_AT_INIT`: Device name

### Optional

* `CAPTURE_START_TIME`: Image capture start time of day HH:mm:ss (or similar)
* `CAPTURE_STOP_TIME`: Image capture stop time of day HH:mm:ss (or similar)
* `CAPTURE_INTERVAL`: Image capture interval HH:mm:ss (or similar)
* `SYNC_START_TIME`: Sync start time of day HH:mm:ss (or similar)
* `SYNC_STOP_TIME`: Sync stop time of day HH:mm:ss (or similar)
* `SYNC_INTERVAL`: Sync interval HH:mm:ss (or similar)
* `API_KEY`: The bearer token for web API authorization
* `EXPOSURE_PROFILE`: Camera exposure profile {Auto, Night, Backlight, 
Spotlight, Snow, Beach, VeryLong}
* `WHITE_BALANCE`: Camera white balance {Auto, Sun, Cloud, Shade}
* `SENSOR_MODE`: Camera sensor mode (0-7)
* `CAMERA_WARMUP`: Camera warmup time (ms)
* `IMAGE_QUALTITY`: Image encoding quality (0-100)
* `IMAGE_ANNOTATION`: Show image information overlay
* `IMAGE_ANNOTATION_SIZE`: Overlay text size (6-160)
* `ROI_X`: Region of interest top left corner, normaised x coordinate (0-1)
* `ROI_Y`: Region of interest top left corner, normaised y coordinate (0-1)
* `ROI_W`: Region of interest normalised width (0-1)
* `ROI_H`: Region of interest normalised height (0-1)
* `IMAGE_WIDTH`: Image width (px)
* `IMAGE_HEIGHT`: Image height (px)
* `SAVE_API_IMAGES`: Upload still images captured through the API to Google 
Photos {true, false}
* `STREAM_CAPTURE_INTERVAL`: Video stream frame capture interval (ms) 
(not used by web api)
* `STREAM_TIMEOUT`: Video stream max run time (s)  (not used by web api)
* `LAPTOP_ENVIRONMENT`: Avoid running Raspberry Pi specific code (`raspistill`) 
{true, false}
* `LAT`: Latitude of camera (deg)
* `LON`: Longitude of camera (deg)

## Deploy

1. Build code

    ```
    dotnet publish .\rabbie-cam\RabbieCam\RabbieCam.csproj -c Release
    ```

2. Balena push

    ```
    balena login
    balena push rabbie-cam --convert-eol
    ```

## Host Directories

volume `data` -> `/var/lib/docker/volumes/<APP ID>_resin-data/_data`
