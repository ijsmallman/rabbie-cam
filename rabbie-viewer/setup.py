from distutils.core import setup

setup(
    name='RabbieViewer',
    version='dev',
    packages=['rabbie_viewer'],
    author="Joe Smallman",
    author_email="ijsmallman@gmail.com",
    description="Simple RabbieCam image and stream viewer",
    entry_points={
          'console_scripts': [
              'rabbieview = rabbie_viewer.__main__:run'
          ]
      }
)
