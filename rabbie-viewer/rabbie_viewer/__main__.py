import asyncio
from http import HTTPStatus
import aiohttp
from aiohttp import ClientSession, MultipartReader
from aiohttp.web_exceptions import HTTPException
from urllib.parse import urljoin
import logging
import json
from typing import ByteString
import cv2
import numpy as np
import matplotlib.pyplot as plt
from dataclasses import dataclass
import ctypes

logger = logging.getLogger(__name__)

@dataclass
class Config:
    endpoint: str
    multipart: bool
    hold_ui: bool

CONFIG = {
    'capture': Config("/api/Camera/CaptureImage", False, True),
    'stream': Config("/api/Camera/LiveStream", True, False)
}

WINDOW_NAME = 'Camera Feed'

class EndException(Exception):
    pass

class Viewer:
    """Simple viewer for visualising image data from RabbieCam cameras."""

    def __init__(self, base_url: str, authorization_token: str) -> None:
        """Instantiate class."""

        headers = {'Authorization': f'Bearer {authorization_token}'}
        self._session = ClientSession(headers=headers)
        self._base_url = base_url

        self._ui_initialised = False
        self._screen_height = None
        self._screen_width = None

    async def __aenter__(self) -> 'Viewer':
        return self

    async def __aexit__(self, *_) -> None:
        await self.close()

    async def close(self) -> None:
        """Close open sessions."""
        await self._session.close()

    async def get(
        self,
        endpoint: str,
        multipart: bool = False,
        params = None
    ) -> ByteString:
        """Make GET request to endpoint."""

        url = urljoin(self._base_url, endpoint)

        byte_count = 0
        async with self._session.get(url, params=params) as resp:

            if resp.status != HTTPStatus.OK:
                text = await resp.json()
                logger.error(f"Failed to retrieve data: {text['title']}.")
                raise HTTPException()
            
            if multipart:
                reader = MultipartReader.from_response(resp)

                while True:
                    part = await reader.next()
                    if part is None:
                        logger.debug("Stream ended.")
                        break
                    data = await part.read()
                    logger.debug(f"Received {len(data)} bytes.")
                    byte_count += len(data)
                    yield data
            else:
                data = await resp.read()
                logger.debug(f"Received {len(data)} bytes.")
                byte_count += len(data)
                yield data
            
            logger.debug(f"Recieved {byte_count} bytes of image data.")
                

    def _init_ui(self) -> None:
        """Initialise the UI"""
        user32 = ctypes.windll.user32
        self._screen_width = user32.GetSystemMetrics(0)
        self._screen_height = user32.GetSystemMetrics(1)

        cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_AUTOSIZE)
        self._ui_initialised = True

    def show(self, image_data: ByteString, hold_ui: bool) -> None:
        """Show image data in GUI."""

        if not self._ui_initialised:
            self._init_ui()

        try:
            image_array = np.asarray(bytearray(image_data), dtype=np.uint8)
            bgr_image = cv2.imdecode(image_array, cv2.IMREAD_COLOR)
            height, width = bgr_image.shape[:2]

            if height > self._screen_height or width > self._screen_width:

                height_sf = float(self._screen_width) / float(height)
                width_sf = float(self._screen_width) / float(width)

                if height_sf > width_sf:
                    sf = 0.75 * width_sf
                else:
                    sf = 0.75 * height_sf

                bgr_image = cv2.resize(
                    bgr_image,
                    (int(width*sf), int(height*sf))
                )
                logger.debug("Image resized.")
        
        except Exception as e:
            logger.error(str(e))
            return False
        
        cv2.imshow(WINDOW_NAME, bgr_image)
        
        if hold_ui:
            timeout = -1
        else:
            timeout = 10

        close = cv2.waitKey(timeout) & 0xFF == ord('q') or \
            cv2.getWindowProperty(WINDOW_NAME, cv2.WND_PROP_VISIBLE) < 1
        if close:
            raise EndException()
        

async def main():
    """Main viewer application."""

    logging.basicConfig(level=logging.DEBUG)

    import argparse 
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'base_url',
        type=str,
        help="ddress of camera"
    )
    parser.add_argument(
        "token",
        type=str,
        help="authorization token"
    )
    parser.add_argument(
        'image_format',
        type=str, 
        choices=list(CONFIG.keys()),
        help="run config"
    )
    parser.add_argument(
        '--image_quality',
        type=int,
        required=False,
        help="image quality [0,100]"
    )
    parser.add_argument(
        '--sensor_mode',
        type=int,
        required=False,
        help="sensor mode [0,7]"
    )
    parser.add_argument(
        '--roi',
        type=str,
        required=False,
        help="ROI in format x,y,w,h"
    )
    parser.add_argument(
        '--image_width',
        type=int,
        required=False,
        help="image width (px)"
    )
    parser.add_argument(
        '--image_height',
        type=int,
        required=False,
        help="image height (px)"
    )
    args = parser.parse_args()

    params={}
    if args.image_quality is not None:
        params['ImageQuality'] = args.image_quality
    if args.sensor_mode is not None:
        params['SensorMode'] = args.sensor_mode
    if args.roi is not None:
        roi = args.roi.split(',')
        params['RoiX'] = roi[0]
        params['RoiY'] = roi[1]
        params['RoiW'] = roi[2]
        params['RoiH'] = roi[3]
    if args.image_width is not None:
        params['ImageWidth'] = args.image_width
    if args.image_height is not None:
        params['ImageHeight'] = args.image_height

    if not len(params.keys()):
        params = None

    image_format = args.image_format
    base_url = args.base_url
    token = args.token

    config = CONFIG[image_format]

    async with Viewer(base_url, token) as viewer:
        try:
            async for image_data in viewer.get(
                config.endpoint, 
                config.multipart,
                params
            ):
                viewer.show(image_data, config.hold_ui)

        except (HTTPException):
            pass
    
        except(EndException):
            pass

    cv2.destroyAllWindows()        

def run():
    """Called by console script."""

    asyncio.run(main())

if __name__ == "__main__":
    run()
