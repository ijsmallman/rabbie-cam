using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Xunit;
using RabbieCam.Utils;

namespace RabbieCam.Tests
{
    public class TimeGenerator
    {
        public static IEnumerable<object[]> TestData
        {
            get
            {
                yield return new object[] {Time.Parse("12:00"), new Time(12, 0, 0)};
                yield return new object[] {Time.Parse("12:00:00"), new Time(12, 0, 0)};
                yield return new object[] {Time.Parse("9:00"), new Time(9, 0, 0)};
                yield return new object[] {Time.Parse("09:00:00"), new Time(9, 0, 0)};
                yield return new object[] {Time.Parse("13:00:00"), new Time(13, 0, 0)};
            }
        }
    }

    public class TestTime
    {   
        [Theory]
        [MemberData(nameof(TimeGenerator.TestData), MemberType = typeof(TimeGenerator))]
        public void TestStringConstructor(Time actual, Time expected)
        {
            Assert.Equal(expected, actual);
        }
    }
}
