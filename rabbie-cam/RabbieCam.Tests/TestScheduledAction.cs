using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Xunit;
using RabbieCam.Utils;

namespace RabbieCam.Tests
{
    public class RunTimeGenerator
    {
        public static IEnumerable<object[]> TestData
        {
            get
            {
                yield return new object[] {new Time(11, 0, 0), new TimeSpan(0, 10, 0)};
                yield return new object[] {new Time(9, 0, 0), new TimeSpan(1, 0, 0)};
                yield return new object[] {new Time(10, 0, 0), new TimeSpan(0, 10, 0)};
                yield return new object[] {new Time(10, 5, 0), new TimeSpan(0, 5, 0)};
                yield return new object[] {new Time(11, 55, 0), new TimeSpan(22, 5, 0)};
                yield return new object[] {new Time(13, 0, 0), new TimeSpan(21, 0, 0)};
                yield return new object[] {new Time(11, 40, 0), new TimeSpan(0, 10, 0)};
                yield return new object[] {new Time(11, 50, 0), new TimeSpan(22, 10, 0)};
                yield return new object[] {new Time(11, 52, 0), new TimeSpan(22, 8, 0)};
            }
        }
    }

    public class TestScheduledAction
    {   
        private readonly ScheduledAction scheduledAction;

        public TestScheduledAction()
        {
            scheduledAction = new ScheduledAction(
                new Time(10, 0, 0),
                new Time(11, 55, 0),
                new TimeSpan(0, 10, 0),
                "test",
                async ()=>{await Task.Delay(1);}
            );
        }

        [Theory]
        [MemberData(nameof(RunTimeGenerator.TestData), MemberType = typeof(RunTimeGenerator))]
        public void TestNextRunInterval(Time time, TimeSpan expected)
        {
            var actual = scheduledAction.NextRunInterval(time);
            Assert.Equal(expected, actual);
        }
    }
}
