using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using Xunit;
using RabbieCam.Sync;

namespace RabbieCam.Tests
{
    public class TestPhotoSync
    {   
        private readonly PhotoSync sync;

        public TestPhotoSync()
        {
            sync = new PhotoSync();
        }

        [Fact]
        public void TestListImages()
        {
            var codeBaseUrl = new Uri(Assembly.GetExecutingAssembly().CodeBase);
            var codeBasePath = Uri.UnescapeDataString(codeBaseUrl.AbsolutePath);
            var dirPath = Path.GetDirectoryName(codeBasePath);
            string relativePath = "../../../Resources/";
            string imagesPath = Path.GetFullPath(Path.Combine(dirPath, relativePath));
            Environment.SetEnvironmentVariable("IMAGES_PATH", imagesPath);

            var imagePaths = sync.ListImages();
            Assert.Equal(1, imagePaths.Count());
        }
    }
}
