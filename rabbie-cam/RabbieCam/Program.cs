﻿using System;
using System.Reflection;
using System.IO;
using System.Text.Json.Serialization;
using NLog;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using RabbieCam.Capture;
using RabbieCam.Sync;
using RabbieCam.Utils;


namespace RabbieCam
{
    public class Program
    {
        public static void Main(string[] args)
        {
            LogManager.LoadConfiguration("nlog.config");
            var logger = LogManager.GetCurrentClassLogger();

            var camera = new RPiCamera();

            var scheduler = new Scheduler();

            // Scheduled image capture
            string captureId = "capture";
            try
            {
                scheduler.ScheduleAction(
                    Time.Parse(Environs.CaptureStart),
                    Time.Parse(Environs.CaptureStop),
                    TimeSpan.Parse(Environs.CaptureInterval),
                    captureId,
                    async () => {
                        try
                        {
                            await camera.CaptureToFileAsync();
                        }
                        catch (CameraBusyException e)
                        {
                            logger.Error(
                                $"Failed to capture image: {e.Message}"
                            );
                        }
                        catch (Exception e)
                        {
                            logger.Error(
                                $"Failed to respond with captured image: " +
                                $"{e.Message}"
                            );
                        }
                    }
                );
            }
            catch (Exception e)
            {
                logger.Warn(
                    $"\"{captureId}\" action not scheduled: {e.Message}"
                );
            }

            // Scheduled image upload
            string uploadId = "upload";
            try
            {
                var sync = new PhotoSync();
                scheduler.ScheduleAction(
                    Time.Parse(Environs.SyncStart),
                    Time.Parse(Environs.SyncStop),
                    TimeSpan.Parse(Environs.SyncInterval),
                    uploadId,
                    sync.SyncImagesAsync
                );
            }
            catch (Exception e)
            {
                logger.Warn(
                    $"\"{uploadId}\" action not scheduled: {e.Message}"
                );
            }

            // Warn if authorization is disabled
            try
            {
                var apiKey = Environs.ApiKey;
            }
            catch (ArgumentNullException)
            {
                logger.Warn("API key not set. Authorization disabled.");
            }

            // Web API
            CreateHostBuilder(args, camera).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args, 
                                                     ICamera camera)
        {
            var builder = Host.CreateDefaultBuilder(args);
            builder.ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.ConfigureServices(services => 
                {
                    services.AddSingleton<ICamera>(camera);
                    services.AddControllers()
                            .AddJsonOptions(options => 
                            {
                                options.JsonSerializerOptions.Converters.Add(
                                    new JsonStringEnumConverter()
                                );
                            });
                

                    services.AddSwaggerGen(c =>
                    {
                        c.SwaggerDoc(
                            "v1", 
                            new OpenApiInfo { 
                                Title = "RabbieCam API", 
                                Version = "v1" 
                            }
                        );

                        c.AddSecurityDefinition(
                            "Bearer", 
                            new OpenApiSecurityScheme
                            {
                                Description = 
                                    @"Token authorization header using the " +
                                    @"Bearer scheme." + 
                                    @"<BR/>Enter `Bearer <token>`" +
                                    @" e.g. `Bearer 12345abcdef`.",
                                Name = "Authorization",
                                In = ParameterLocation.Header,
                                Type = SecuritySchemeType.ApiKey,
                                Scheme = "Bearer"
                            }
                        );

                        c.AddSecurityRequirement(new OpenApiSecurityRequirement
                        {
                            {
                                new OpenApiSecurityScheme
                                    {
                                        Reference = new OpenApiReference
                                        {
                                            Type = ReferenceType.SecurityScheme,
                                            Id = "Bearer"
                                        }
                                    },
                                    new string[] {}
                            }
                        });

                        // Set the comments path for the Swagger JSON and UI.
                        var xmlFileName = 
                            Assembly.GetExecutingAssembly().GetName().Name;
                        var xmlFile = $"{xmlFileName}.xml";
                        var xmlPath = Path.Combine(
                            AppContext.BaseDirectory, 
                            xmlFile
                        );
                        c.IncludeXmlComments(xmlPath);

                    });
                })
                .Configure(app =>
                {
                    var env = app.ApplicationServices
                        .GetRequiredService<IWebHostEnvironment>();
                    var config = app.ApplicationServices
                        .GetRequiredService<IConfiguration>();

                    if (env.IsDevelopment())
                    {
                        app.UseDeveloperExceptionPage();
                    }

                    // Enable middleware to serve generated Swagger as a 
                    // JSON endpoint.
                    app.UseSwagger();

                    // Enable middleware to serve swagger-ui 
                    // (HTML, JS, CSS, etc.), specifying the Swagger 
                    // JSON endpoint.
                    app.UseSwaggerUI(c =>
                    {
                        c.SwaggerEndpoint(
                            "/swagger/v1/swagger.json", 
                            "RabbieCam API V1"
                        );
                        c.RoutePrefix = string.Empty;
                    });

                    app.UseHttpsRedirection();

                    app.UseRouting();

                    // Simple token auth in filter
                    //app.UseAuthentication();
                    //app.UseAuthorization();

                    app.UseEndpoints(endpoints =>
                    {
                        endpoints.MapControllers();
                    });
                });
            });
            return builder;
        }
    }
}
