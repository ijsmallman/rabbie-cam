using System;
using System.Timers;
using System.Threading.Tasks;
using NLog;

namespace RabbieCam.Utils
{
    /// <summary>
    /// Action scheduled to occur on a regular basis.
    /// </summary>
    public class ScheduledAction
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Action to be triggered on schedule.
        /// </summary>
        private Func<Task> func;

        /// <summary>
        /// Timer used to schedule actions.
        /// </summary>
        private Timer timer = default!;

        /// <summary>
        /// Human readable name for action.
        /// </summary>
        public string ID {get; private set;}

        /// <summary>
        /// First trigger time of action.
        /// </summary>
        public Time From {get; private set;}

        /// <summary>
        /// Last trigger time. 
        /// time
        /// </summary>
        /// <remarks>
        /// Actions are only triggered up to this time. The actual last trigger
        /// time depends upon the trigger interval.
        /// </remarks>
        public Time To {get; private set;}

        /// <summary>
        /// Trigger interval.
        /// </summary>
        public TimeSpan Interval{get; private set;}
        
        /// <summary>
        /// Instantiate scheduled action class.
        /// </summary>
        /// <param name="from">First trigger time.</param>
        /// <param name="to">Last trigger time.</param>
        /// <param name="interval">Trigger interval.</param>
        /// <param name="id">Human readable name for action.</param>
        /// <param name="func">Action to trigger on schedule.</param>
        public ScheduledAction(Time from, 
                               Time to, 
                               TimeSpan interval, 
                               string id, 
                               Func<Task> func)
        {
            ID = id;
            From = from;
            To = to;
            Interval = interval;
            this.func = func;
        }

        /// <summary>
        /// Start the action shedule.
        /// </summary>
        public void Start()
        {
            var delta = NextRunInterval(new Time(DateTime.UtcNow));
            timer = new Timer(delta.Duration().TotalMilliseconds);
            timer.Elapsed += TimerEvent;
            timer.AutoReset = true;
            timer.Start();
            logger.Debug($"Action \"{ID}\" started.");
        }

        /// <summary>
        /// Stop the action schedule.
        /// </summary>
        public void Stop()
        {
            timer.Stop();
            logger.Debug($"Action \"{ID}\" stopped.");
        }

        /// <summary>
        /// Callback triggered when timer elapses. This method is called every
        /// trigger interval.
        /// </summary>
        private void TimerEvent(object sender, EventArgs e)
        {
            Task task = CallFuncAsync();
            var delta = NextRunInterval(new Time(DateTime.UtcNow));
            timer.Interval = delta.Duration().TotalMilliseconds;
            logger.Debug($"Action \"{ID}\" triggered.");
        }

        /// <summary>
        /// Call the action asynchronously.
        /// </summary>
        private async Task CallFuncAsync()
        {
            try
            {
                await func();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }
        }

        /// <summary>
        /// Compute interval until next trigger time.
        /// </summary>
        /// <param name="time">Compute interval from this time.</param>
        public TimeSpan NextRunInterval(Time time)
        {
            DateTime t0 = time.TimeStamp;
            DateTime t = From.TimeStamp;

            // Find next invokation on same day
            while (t < t0)
            {
                t += Interval;
            }

            // Compute wait time
            var delta = t.Subtract(t0);

            // Action should be called now! 
            // Skip and wait for next time around
            if (delta == new TimeSpan(0,0,0))
            {
                delta = delta.Add(Interval);
            }

            // Check that the scheduled time after last invokation time
            if (t0 + delta > To.TimeStamp)
            {
                // Wait for next day
                t = From.TimeStamp.Add(new TimeSpan(1, 0, 0, 0));
                delta = t.Subtract(t0);
            }

            logger.Debug($"Interval to next \"{ID}\" action trigger: {delta}.");

            return delta;
        }
    }
}
