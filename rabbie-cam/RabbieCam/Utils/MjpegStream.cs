using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NLog;

namespace RabbieCam.Utils
{
    public class MjpegStream: IActionResult
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly string boundary = "frame";
        private static readonly string contentType = 
            "multipart/x-mixed-replace;boundary=" + boundary;
        private static readonly string crlf = "\r\n";
        private static readonly byte[] crlfBytes = Encoding.UTF8.GetBytes(crlf);

        /// <summary>
        /// Function to retrieve next image in stream.
        /// </summary>
        private readonly Func<CancellationToken, Task<byte[]>> GetImage;

        /// <summary>
        /// Function that completes with true when there is data available and
        /// false when no more data is available.
        /// </summary>
        private readonly Func<CancellationToken, ValueTask<bool>> ImageAvailable;

        /// <summary>
        /// Function to call when connection is closed.
        /// </summary>
        private readonly Action Dispose;

        private int byteCount = 0;

        /// <summary>
        /// Buffer for camera frames.
        /// </summary>
        /// <remarks>
        /// Limit to 2 frames.
        /// </remarks>
        private BlockingCollection<byte[]> queue = 
            new BlockingCollection<byte[]>(2);

        /// <summary>
        /// Instantiate new MJPEG stream.
        /// </summary>
        /// <param name="GetImage">
        /// Function to retrieve next image in stream.
        /// </param>
        /// <param name="ImageAvailable">
        /// Function that completes with true when there is data available and 
        /// false when no more data is available.
        /// </param>
        /// <param name="Dispose">
        /// Function to call when connection is closed.
        /// </param>
        public MjpegStream(
            Func<CancellationToken, Task<byte[]>> GetImage,
            Func<CancellationToken, ValueTask<bool>> ImageAvailable, 
            Action Dispose
        )
        {
            this.GetImage = GetImage;
            this.ImageAvailable = ImageAvailable;
            this.Dispose = Dispose;
            logger.Debug("Created new MJPEG stream.");
        }

        /// <summary>
        /// Build response header message.
        /// </summary>
        /// <param name="bodySize">Bytes in body content.</param>
        private string Header(int bodySize)
        {
            var builder = new StringBuilder();

            builder.Append($"--{boundary}");
            builder.Append(crlf);
            builder.Append("Content-Type: image/jpeg");
            builder.Append(crlf);
            builder.Append($"Content-Length: {bodySize}");
            builder.Append(crlf);
            builder.Append(crlf);

            return builder.ToString();
        }

        /// <summary>
        /// Write out MJPEG images as a multi-part response until connection is
        /// closed by the client or the camera.
        /// </summary>
        public async Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.ContentType = contentType;

            var outputStream = context.HttpContext.Response.Body;
            var cancellationToken = context.HttpContext.RequestAborted;

            try
            {
                while (!cancellationToken.IsCancellationRequested &&
                       await ImageAvailable(cancellationToken))
                {
                    var imageBytes = await GetImage(cancellationToken);
                    // logger.Debug("Retrieved frame from buffer");

                    var headerData = Encoding.UTF8.GetBytes(
                        Header(imageBytes.Length)
                    );
                    await outputStream.WriteAsync(
                        headerData, 
                        0, 
                        headerData.Length, 
                        cancellationToken
                    );
                    await outputStream.WriteAsync(
                        imageBytes, 
                        0, 
                        imageBytes.Length,
                        cancellationToken
                    );
                    await outputStream.WriteAsync(
                        crlfBytes,
                        0,
                        crlfBytes.Length,
                        cancellationToken
                    );

                    byteCount += (
                        headerData.Length + 
                        imageBytes.Length + 
                        crlfBytes.Length
                    );
                }
            }
            catch (Exception e) when (e is TaskCanceledException ||
                                      e is OperationCanceledException)
            {
                logger.Warn($"Stream cancelled.");
            }
            catch (Exception e)
            {
                logger.Error($"Unexpected error: {e.Message}");
            }
            finally
            {
                try
                {
                    var finalBoundaryData = Encoding.UTF8.GetBytes(
                        $"--{boundary}--"
                    );
                    await outputStream.WriteAsync(
                        finalBoundaryData,
                        0,
                        finalBoundaryData.Length
                    );
                    await outputStream.WriteAsync(
                        crlfBytes,
                        0,
                        crlfBytes.Length
                    );
                    byteCount += (finalBoundaryData.Length + crlfBytes.Length);

                    logger.Debug($"Stream terminated. {byteCount} bytes sent.");
                }
                catch (Exception e)
                {
                    logger.Error(
                        $"Failed to write final boundary: {e.Message}"
                    );
                }

                // Close any open connections
                Dispose();
            }
        }
    }
}