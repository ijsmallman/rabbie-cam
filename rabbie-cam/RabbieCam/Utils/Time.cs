using System;
using System.Globalization;

namespace RabbieCam.Utils
{
    /// <summary>
    /// Wrapper for time of day.
    /// </summary>
    public class Time
    {
        /// <summary>
        /// Underlying object for storing time.
        /// </summary>
        public DateTime TimeStamp {get; private set;}

        /// <summary>
        /// Create new time object.
        /// </summary>
        /// <param name="hours">Hours past midnight.</param>
        /// <param name="minutes">Minutes past the hour.</param>
        /// <param name="seconds">Seconds past the minute.</param>
        public Time(int hours, int minutes, int seconds)
        {
            TimeStamp = new DateTime(1, 1, 1, hours, minutes, seconds);
        }
        
        /// <summary>
        /// Create new time object, stripping out the date information.
        /// </summary>
        /// <param name="timestamp">Time stamp.</param>
        public Time(DateTime timestamp)
        {
            TimeStamp = new DateTime(
                1, 1, 1, timestamp.Hour, timestamp.Minute, timestamp.Second
            );
        }

        /// <summary>
        /// Time from string.
        /// </summary>
        /// <param name="timestamp">Time stamp.</param>
        public static Time Parse(string timestamp)
        {
            return new Time(
                DateTime.Parse(
                    timestamp, 
                    new CultureInfo("en-GB"),
                    DateTimeStyles.NoCurrentDateDefault
                )
            );
        }

        /// <summary>
        /// Create new time object.
        /// </summary>
        /// <param name="timestamp">Time interval since midnight.</param>
        public Time(TimeSpan timestamp)
        {
            TimeStamp = new DateTime(1, 1, 1, 0, 0, 0).Add(timestamp);
        }

        /// <summary>
        /// Format time into human readable string.
        /// </summary>
        public override string ToString()
        {
            return $"{TimeStamp.Hour:00}:" +
                   $"{TimeStamp.Minute:00}:" +
                   $"{TimeStamp.Second:00}";
        }

        /// <summary>
        /// Less than time comparison.
        /// </summary>
        public static bool operator < (Time t1, Time t2)
        {
            return t1.TimeStamp < t2.TimeStamp;   
        }

        /// <summary>
        /// Less than or equal to time comparison.
        /// </summary>
        public static bool operator <= (Time t1, Time t2)
        {
            return t1.TimeStamp <= t2.TimeStamp;
        }

        /// <summary>
        /// Greater than time comparison.
        /// </summary>
        public static bool operator > (Time t1, Time t2)
        {
            return t1.TimeStamp > t2.TimeStamp;
        }

        /// <summary>
        /// Greater than or equal to time comparison.
        /// </summary>
        public static bool operator >= (Time t1, Time t2)
        {
            return t1.TimeStamp >= t2.TimeStamp;
        }

#nullable enable
        /// <summary>
        /// Compare time equality.
        /// </summary>
        public override bool Equals(object? obj)
        {
            if(obj == null)
            { 
                return false;
            }

            var other = obj as Time;
            
            return TimeStamp == other.TimeStamp;
        }
#nullable disable

        /// <summary>
        ///  Hash code for time.
        /// </summary>
        public override int GetHashCode()
        {
            return TimeStamp.GetHashCode();
        }

        /// <summary>
        /// Compare time equality.
        /// </summary>
        public static bool operator == (Time t1, Time t2)
        {
            return t1.TimeStamp == t2.TimeStamp;
        }

        /// <summary>
        /// Compare time equality.
        /// </summary>
        public static bool operator != (Time t1, Time t2)
        {
            return t1.TimeStamp != t2.TimeStamp;
        }

        /// <summary>
        /// Add times.
        /// </summary>
        public static Time operator + (Time t, TimeSpan dt)
        {
            return new Time(t.TimeStamp.Add(dt));
        }

        /// <summary>
        /// Subtract times.
        /// </summary>
        public static Time operator - (Time t, TimeSpan dt)
        {
            return new Time(t.TimeStamp.Subtract(dt));
        }

        /// <summary>
        /// Subtract interval from time.
        /// </summary>
        public static TimeSpan operator - (Time t1, Time t2)
        {
            return t1.TimeStamp.Subtract(t2.TimeStamp);
        }
    }
}
