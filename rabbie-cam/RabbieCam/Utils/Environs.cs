using System;
using RabbieCam.Capture;

namespace RabbieCam.Utils
{
    /// <summary>
    /// Application environment variables.
    /// </summary>
    /// <remarks>
    /// This getters intentially throw errors if the variable is not set or 
    /// cannot be read. The default values are defined in the appropriate class.
    /// </remarks>
    public static class Environs 
    {
        /// <summary>
        /// Get environment variable, enforcing it has been set.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string GetEnvironmentVariable(string variable)
        {
            var output = Environment.GetEnvironmentVariable(variable);
            return output ?? throw new ArgumentNullException(
                variable, 
                $"Environment variable not set."
            );
        }

        /// <summary>
        /// Directory containing secret files (e.g. credentials).
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string SecretsPath
        {
            get => GetEnvironmentVariable("SECRETS_PATH");
        }

        /// <summary>
        /// Path to Google credentials.json (usually in `SECRETS_PATH`).
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string CredentialsPath
        {
            get => GetEnvironmentVariable("CREDENTIALS_PATH");
        }

        /// <summary>
        /// Directory to store captured images.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>        
        public static string ImagesPath
        {
            get => GetEnvironmentVariable("IMAGES_PATH");
        }

        /// <summary>
        /// Username for uploading images to Google Photos.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string UserName
        {
            get => GetEnvironmentVariable("USER_NAME");
        }

        /// <summary>
        ///  Device name.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string DeviceName
        {
            get => GetEnvironmentVariable("BALENA_DEVICE_NAME_AT_INIT");
        }

        /// <summary>
        /// Capture start time of day.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string CaptureStart
        {
            get => GetEnvironmentVariable("CAPTURE_START_TIME");
        } 

        /// <summary>
        /// Capture stop time of day.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string CaptureStop
        {
            get => GetEnvironmentVariable("CAPTURE_STOP_TIME");
        }

        /// <summary>
        /// Capture interval.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string CaptureInterval
        {
            get => GetEnvironmentVariable("CAPTURE_INTERVAL");
        }

        /// <summary>
        /// Sync start time of day.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string SyncStart
        {
            get => GetEnvironmentVariable("SYNC_START_TIME");
        } 

        /// <summary>
        /// Sync stop time of day.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string SyncStop
        {
            get => GetEnvironmentVariable("SYNC_STOP_TIME");
        }

        /// <summary>
        /// Sync interval.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string SyncInterval
        {
            get => GetEnvironmentVariable("SYNC_INTERVAL");
        }

        /// <summary>
        /// API key.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        public static string ApiKey
        {
            get => GetEnvironmentVariable("API_KEY");
        }

        /// <summary>
        /// Camera exposure profile.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        public static ExposureProfile ExposureProfile
        {
            get
            {
                var val = GetEnvironmentVariable("EXPOSURE_PROFILE");
                ExposureProfile parsedVal;
                if (!Enum.TryParse<ExposureProfile>(val, true, out parsedVal))
                {
                    // Throw exception so deal with defaults in CaptureSettings
                    throw new FormatException("EXPOSURE_PROFILE");
                }
                return parsedVal;
            }
        }

        /// <summary>
        /// Camera white balance.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        public static WhiteBalance WhiteBalance
        {
            get
            {
                var val = GetEnvironmentVariable("WHITE_BALANCE");
                WhiteBalance parsedVal;
                if (!Enum.TryParse<WhiteBalance>(val, true, out parsedVal))
                {
                    // Throw exception so deal with defaults in CaptureSettings
                    throw new FormatException("WHITE_BALANCE");
                }
                return parsedVal;
            }
        }

        /// <summary>
        /// Camera sensor mode.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static int SensorMode
        {
            get
            {
                var val = GetEnvironmentVariable("SENSOR_MODE");
                return int.Parse(val);
            }
        }

        /// <summary>
        /// Camera warm up time (ms).
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static int CameraWarmUp
        {
            get
            {
                var val = GetEnvironmentVariable("CAMERA_WARMUP");
                return int.Parse(val);
            }
        }

        /// <summary>
        /// Image encoding quality [0-100].
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static int ImageQuality
        {
            get
            {
                var val = GetEnvironmentVariable("IMAGE_QUALITY");
                return int.Parse(val);
            }
        }

        /// <summary>
        /// Region of interest 'x' normalised coordinate [0-1].
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static double RoiX
        {
            get
            {
                var val = GetEnvironmentVariable("ROI_X");
                return double.Parse(val);
            }
        }

        /// <summary>
        /// Region of interest 'y' normalised coordinate [0-1].
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static double RoiY
        {
            get
            {
                var val = GetEnvironmentVariable("ROI_Y");
                return double.Parse(val);
            }
        }

        /// <summary>
        /// Region of interest width, normalised to image size [0-1].
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static double RoiW
        {
            get
            {
                var val = GetEnvironmentVariable("ROI_W");
                return double.Parse(val);
            }
        }

        /// <summary>
        /// Region of interest height, normalised to image size [0-1].
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static double RoiH
        {
            get
            {
                var val = GetEnvironmentVariable("ROI_H");
                return double.Parse(val);
            }
        }

        /// <summary>
        /// Enable image annotaion.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        public static bool ImageAnnotation
        {
            get
            {
                var val = GetEnvironmentVariable("IMAGE_ANNOTATION");
                return Boolean.Parse(val);
            }
        }

        /// <summary>
        /// Save images captured from the REST API.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        public static bool SaveApiImages
        {
            get
            {
                var val = GetEnvironmentVariable("SAVE_API_IMAGES");
                return Boolean.Parse(val);
            }
        }

        /// <summary>
        /// Stream capture interval (ms).
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static int StreamCaptureInterval
        {
            get
            {
                var val = GetEnvironmentVariable("STREAM_CAPTURE_INTERVAL");
                return int.Parse(val);
            }
        }

        /// <summary>
        /// Stream time out (ms).
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static int StreamTimeOut
        {
            get
            {
                var val = GetEnvironmentVariable("STREAM_TIMEOUT");
                return int.Parse(val);
            }
        }

        /// <summary>
        /// Image width (px).
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static int ImageWidth
        {
            get
            {
                var val = GetEnvironmentVariable("IMAGE_WIDTH");
                return int.Parse(val);
            }
        }

        /// <summary>
        /// Image height (px).
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static int ImageHeight
        {
            get
            {
                var val = GetEnvironmentVariable("IMAGE_HEIGHT");
                return int.Parse(val);
            }
        }

        /// <summary>
        /// Image annotation text size (pt).
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static int ImageAnnotationSize
        {
            get
            {
                var val = GetEnvironmentVariable("IMAGE_ANNOTATION_SIZE");
                return int.Parse(val);
            }
        }

        /// <summary>
        /// Run code on laptop environment.
        /// </summary>
        /// <remarks>
        /// This avoids the raspberry pi specific code.
        /// </remarks>
        public static bool LaptopEnvironment
        {
            get
            {
                try
                {
                    var val = GetEnvironmentVariable("LAPTOP_ENVIRONMENT");
                    return Boolean.Parse(val);
                }
                catch
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Region of interest height, normalised to image size.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static double Lat
        {
            get
            {
                var val = GetEnvironmentVariable("LAT");
                return double.Parse(val);
            }
        }

        /// <summary>
        /// Region of interest height, normalised to image size.
        /// </summary>
        /// <exception cref="System.ArgumentNullException">
        /// Environment variable not set.
        /// </exception>
        /// <exception cref="System.FormatException">
        /// Cant parse string to type.
        /// </exception>
        /// <exception cref="System.OverflowException">
        /// Value less than System.Int32.MinValue or greater than 
        /// System.Int32.MaxValue.
        /// </exception>
        public static double Lon
        {
            get
            {
                var val = GetEnvironmentVariable("LON");
                return double.Parse(val);
            }
        }
    }
}
