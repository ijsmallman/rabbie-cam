using System;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;
using NLog;

namespace RabbieCam.Utils
{
    /// <summary>
    /// Manager for multiple simultaneous scheduled actions.
    /// </summary>
    public class Scheduler
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Event to allow callers to wait for schedule to finish.
        /// </summary>
        private ManualResetEventSlim end = new ManualResetEventSlim();

        /// <summary>
        /// Dictionary of action ID->scheduled action registered with scheduler
        /// </summary>
        /// <remarks>
        /// Scheduled actions are not necessarily currently running. 
        /// </remarks>
        private Dictionary<string, ScheduledAction> actions = 
            new Dictionary<string, ScheduledAction> ();

        /// <summary>
        /// Instantiate scheduler.
        /// </summary>
        public Scheduler() {}

        /// <summary>
        /// Schedule a new action to be triggered regularly.
        /// </summary>
        /// <param name="from">First trigger time.</param>
        /// <param name="to">Last trigger time.</param>
        /// <param name="interval">Trigger interval.</param>
        /// <param name="id">Human readable name for action.</param>
        /// <param name="func">Action to trigger on schedule.</param>
        public void ScheduleAction(Time from, 
                                   Time to, 
                                   TimeSpan interval, 
                                   string id, 
                                   Func<Task> func) 
        {
            var scheduledAction = new ScheduledAction(from, to, interval, 
                                                      id, func);
            scheduledAction.Start();
            actions.Add(id, scheduledAction);
            logger.Debug(
                $"Added action \"{id}\" to run from {from} " + 
                $"to {to} with interval {interval}."
            );
        }

        /// <summary>
        /// Remove action from schedule.
        /// </summary>
        /// <param name="id">Human readable name for action.</param>
        public void UnscheduleAction(string id)
        {
            if(actions.TryGetValue(id, out var action))
            {
                try
                {
                    action.Stop();
                    actions.Remove(id);
                    logger.Debug($"Action \"{id}\" unscheduled.");
                }
                catch (Exception e)
                {
                    logger.Error(
                        $"Failed to unschedule action \"{id}\": {e.Message}"
                    );
                }
            }
            else
            {
                logger.Warn(
                    $"Action \"{id}\" not a scheduled action, can't unschedule."
                );
            }
        }

        /// <summary>
        /// Wait for the scheduler to exit.
        /// </summary>
        public void Wait()
        {
            logger.Debug("Waiting for scheduler to end.");
            end.Wait();
        }
    }
}
