using System;
using System.Text;
using System.ComponentModel.DataAnnotations;
using NLog;

namespace RabbieCam.Capture
{
    public class CameraStreamSettings : CameraSettings
    {
        private static Logger logger = LogManager.GetCurrentClassLogger(); 

        /// <summary>
        /// Frame capture interval (ms).
        /// </summary>
        [Range(30, Int32.MaxValue, ErrorMessage="Interval must be greater than {1}ms.")]
        public int StreamCaptureInterval {get; set;}

        /// <summary>
        /// Stream timeout (ms).
        /// </summary>
        [Range(1, Int32.MaxValue, ErrorMessage="Timeout must be greater than 1s")]
        public int StreamTimeOut {get; set;}

        public CameraStreamSettings()
        {
            // Default settings
            ExposureProfile = ExposureProfile.Auto;
            WhiteBalance = WhiteBalance.Auto;
            SensorMode = 0;
            CameraWarmUp = 500; // This is not used
            ImageQuality = 50; // Try to save bandwidth
            RoiX = 0.0;
            RoiY = 0.0;
            RoiW = 1.0;
            RoiH = 1.0;
            ImageAnnotation = false;
            ImageAnnotationSize = 32;
            StreamCaptureInterval = 250;
            StreamTimeOut = 60000;
            ImageWidth = 507;
            ImageHeight = 380;
        }
        
        /// <summary>
        /// Read camera settings from environment variables.
        /// </summary>
        public static CameraStreamSettings FromEnvirons()
        {
            var settings = new CameraStreamSettings();
            CameraSettings.FromEnvirons(settings);
            settings.TrySetFromEnviron(settings, "StreamCaptureInterval");
            settings.TrySetFromEnviron(settings, "StreamTimeOut");
            logger.Debug("Stream settings loaded from environment variables.");
            return settings;
        }

        /// <summary>
        /// Generate argument configuration string for <c>raspistill</c> 
        /// camera script.
        /// </summary>
        /// <param name="filePath">File path for output images.</param>
        /// <param name="timeout">Include the stream timeout parameter.</param>
        public override string Args(string filePath, bool timeout = true)
        {
            var builder = new StringBuilder();
            var basicArgs = base.Args(filePath, false);
            
            builder.Append(basicArgs);
            if (timeout)
            {
                builder.Append($" -t {StreamTimeOut}");
            }
            builder.Append($" -tl {StreamCaptureInterval}");
        
            return builder.ToString();
        }

        /// <summary>
        /// Write out camera settings to string.
        /// </summary>
        public override string ToString()
        {
            var baseString = base.ToString();

            // Remove ")"
            var trimmed = baseString.Remove(baseString.Length-1, 1); 

            var builder = new StringBuilder();
            builder.Append(trimmed);
            builder.Append(",");
            builder.Append($"StreamCaptureInterval={StreamCaptureInterval},");
            builder.Append($"StreamTimeOut={StreamTimeOut})");

            return builder.ToString();
        }
    }
}