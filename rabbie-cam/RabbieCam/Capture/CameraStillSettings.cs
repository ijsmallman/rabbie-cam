using System;
using NLog;
using RabbieCam.Utils;

namespace RabbieCam.Capture
{
    public class CameraStillSettings : CameraSettings
    {
        private static Logger logger = LogManager.GetCurrentClassLogger(); 
        
        public CameraStillSettings()
        {
            // Default settings
            ExposureProfile = ExposureProfile.Auto;
            WhiteBalance = WhiteBalance.Auto;
            SensorMode = 0; // Auto resolution
            CameraWarmUp = 2000; // Recommended by raspistill
            ImageQuality = 75;
            RoiX = 0.0;
            RoiY = 0.0;
            RoiW = 1.0;
            RoiH = 1.0;
            ImageAnnotation = true;
            ImageAnnotationSize = 32;
            ImageWidth = null;
            ImageHeight = null;
        }

        /// <summary>
        /// Read camera settings from environment variables.
        /// </summary>
        public static CameraStillSettings FromEnvirons()
        {
            var settings = new CameraStillSettings();
            CameraSettings.FromEnvirons(settings);
            return settings;
        }
    }
}