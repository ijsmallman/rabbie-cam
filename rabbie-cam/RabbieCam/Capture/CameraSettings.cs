using System;
using System.Text;
using System.Reflection;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using NLog;
using RabbieCam.Utils;

namespace RabbieCam.Capture
{
    /// <summary>
    /// Supported camera exposure settings.
    /// </summary>
    public enum ExposureProfile
    {
        Auto,  // Default
        Night,
        Backlight,
        Spotlight,
        Snow,
        Beach,
        VeryLong
    }

    /// <summary>
    /// Supported camera white balance settings.
    /// </summary>
    public enum WhiteBalance
    {
        Auto,  // Default
        Sun,
        Cloud,
        Shade
    }

    /// <summary>
    /// Camera settings ecapsulation class.
    /// </summary>
    public abstract class CameraSettings
    {
        private static Logger logger = LogManager.GetCurrentClassLogger(); 

        /// <summary>
        /// Image encoding - Only JPEG supported currently.
        /// </summary>
        public readonly string Encoding = "jpeg";

        /// <summary>
        /// Camera exposure setting.
        /// </summary>
        public ExposureProfile ExposureProfile {get; set;}

        /// <summary>
        /// Camera white balance setting.
        /// </summary>
        public WhiteBalance WhiteBalance {get; set;}

        /// <summary>
        /// Camera resolution setting.
        /// </summary>
        [Range(0, 7, ErrorMessage="Mode must be in the range [{1},{2}]")]
        public int SensorMode {get; set;}

        /// <summary>
        /// Time (ms) to adjust exposure and white balance before image is captured.
        /// </summary>
        [Range(1, 10000, ErrorMessage="Warmup must be in the range [{1},{2}]")]
        public int CameraWarmUp {get; set;}

        /// <summary>
        /// Image encoding quality.
        /// </summary>
        [Range(0, 100, ErrorMessage="Quality must be in the range [{1},{2}]")]
        public int ImageQuality {get; set;}

        /// <summary>
        /// Region of interest top left 'x' coordinate, normalised to size of image.
        /// </summary>
        [Range(0, 1, ErrorMessage="Coordinates must be in the range [{1},{2}]")]
        public double RoiX {get; set;} = 0.0;

        /// <summary>
        /// Region of interest top left 'y' coordinate, normalised to size of image.
        /// </summary>
        [Range(0, 1, ErrorMessage="Coordinates must be in the range [{1},{2}]")]
        public double RoiY {get; set;} = 0.0;

        /// <summary>
        /// Region of interest width, normalised to size of image.
        /// </summary>
        [Range(0, 1, ErrorMessage="Coordinates must be in the range [{1},{2}]")]
        public double RoiW {get; set;} = 1.0;

        /// <summary>
        /// Region of interest height, normalised to size of image.
        /// </summary>
        [Range(0, 1, ErrorMessage="Coordinates must be in the range [{1},{2}]")]
        public double RoiH {get; set;} = 1.0;

        /// <summary>
        /// Display timestamp and camera name overlay on image.
        /// </summary>
        public bool ImageAnnotation {get; set;}

        /// <summary>
        /// Image annotation text size.
        /// </summary>
        [Range(6, 160, ErrorMessage="Size must be in the range [{1},{2}]")]
        public int ImageAnnotationSize {get; set;}

        /// <summary>
        /// Image width (px).
        /// </summary>
        public int? ImageWidth {get; set;} = null;

        /// <summary>
        /// Image height (px).
        /// </summary>
        public int? ImageHeight {get; set;} = null;


        /// <summary>
        /// Load settings from environment variables using reflection.
        /// </summary>
        public bool TrySetFromEnviron(CameraSettings settings, string property)
        {
            var settingsType = settings.GetType();
            var settingsPropertyInfo = settingsType.GetProperty(property);
            if (settingsPropertyInfo is null)
            {
                logger.Warn($"Unrecognised setting: \"{property}\".");
                return false;
            }

            var environsType = typeof(Environs);
            var environsPropertyInfo = environsType.GetProperty(property);
            if (environsPropertyInfo is null)
            {
                logger.Warn(
                    $"Unrecognised environment variable: \"{property}\"."
                );
                return false;
            }

            try
            {
                var value = environsPropertyInfo.GetValue(null, null);
                settingsPropertyInfo.SetValue(settings, value);
            }
            catch (Exception e) when (e is ArgumentNullException || 
                                      e is FormatException ||
                                      e is OverflowException ||
                                      e is TargetInvocationException)
            {
                logger.Warn(
                    $"Failed to parse {property} from environment " +
                    "varliable. Using default value: " +
                    $"{settingsPropertyInfo?.GetValue(settings)}."
                );
                return false;
            }

            return true;
        }

        /// <summary>
        /// Write out camera settings to string.
        /// </summary>
        public override string ToString()
        {
            var builder = new StringBuilder();
            builder.Append("CameraSettings(");
            builder.Append($"ExposureProfile={ExposureProfile},");
            builder.Append($"SensorMode={SensorMode},");
            builder.Append($"WhiteBalance={WhiteBalance},");
            builder.Append($"WarmUp={CameraWarmUp},");
            builder.Append($"ImageQuality={ImageQuality},");
            builder.Append(
                $"ROI=({RoiX:0.##},{RoiY:0.##},{RoiW:0.##},{RoiH:0.##}),"
            );
            if (ImageWidth is null)
            {
                builder.Append("ImageWidth=null,");
            }
            else
            {
                builder.Append($"ImageWidth={ImageWidth},");
            }
            if (ImageHeight is null)
            {
                builder.Append("ImageHeight=null,");
            }
            else
            {
                builder.Append($"ImageHeight={ImageHeight},");
            }
            builder.Append($"ImageAnnotation={ImageAnnotation},");
            builder.Append($"ImageAnnotationSize={ImageAnnotationSize}");
            builder.Append(")");
            return builder.ToString();
        }

        /// <summary>
        /// Generate argument configuration string for <c>raspistill</c> 
        /// camera script.
        /// </summary>
        /// <param name="filePath">File path for output image.</param>
        /// <param name="timeout">Include the warmup time parameter.</param>
        public virtual string Args(string filePath, bool timeout = true)
        {
            var builder = new StringBuilder();

            // Output file
            builder.Append($"-o {filePath}");
            builder.Append(" --nopreview");
                
            // Annotation
            if (ImageAnnotation)
            {
                var annotation = $"{Environs.DeviceName} %Y-%m-%d %X %Z";
                builder.Append(
                    $" -ae {ImageAnnotationSize},0xff,0x808000 -a 1036 -a \"{annotation}\""
                );
            }

            // Resolution
            builder.Append($" -md {SensorMode}");

            // White balance
            builder.Append(
                $" -awb {WhiteBalance.ToString("G").ToLower()}"
            );

            // Exposure
            builder.Append(
                $" -ex {ExposureProfile.ToString("G").ToLower()}"
            );
            
            // ROI
            builder.Append(
                $" -roi {RoiX:0.##},{RoiY:0.##},{RoiW:0.##},{RoiH:0.##}"
            );

            // Width
            if (!(ImageWidth is null))
            {
                builder.Append(
                    $" -w {ImageWidth}"
                );
            }

            // Height
            if (!(ImageHeight is null))
            {
                builder.Append(
                    $" -h {ImageHeight}"
                );
            }

            // Quality
            builder.Append($" -q {ImageQuality}");

            if (timeout)
            {
                //Warmup
                builder.Append($" -t {CameraWarmUp}");
            }

            return builder.ToString();
        }

        /// <summary>
        /// Read camera settings from environment variables.
        /// </summary>
        public static void FromEnvirons(CameraSettings settings)
        {
            settings.TrySetFromEnviron(settings, "ExposureProfile");

            // Override exposure profile if lat & lon set, and after dark
            try
            {
                var lat = Environs.Lat;
                var lon = Environs.Lon;
                var now = DateTime.UtcNow;
                var solarEvent = SolarCalculator.Calculate(
                    now.Year,
                    now.Month,
                    now.Day,
                    lat,
                    lon,
                    "UTC"
                );

                var sunrise = solarEvent.Sunrise.UtcDateTime;
                var sunset = solarEvent.Sunset.UtcDateTime;

                if (now < sunrise || now > sunset)
                {
                    settings.ExposureProfile = ExposureProfile.Night;
                    logger.Debug(
                        "Sun not risen, exposure set to night mode."
                    );
                }
            }
            catch
            {
                // Lat/Lon not set, don't override exposure
            }

            settings.TrySetFromEnviron(settings, "WhiteBalance");
            settings.TrySetFromEnviron(settings, "SensorMode");
            settings.TrySetFromEnviron(settings, "CameraWarmUp");
            settings.TrySetFromEnviron(settings, "ImageQuality");
            settings.TrySetFromEnviron(settings, "ImageAnnotation");
            settings.TrySetFromEnviron(settings, "ImageAnnotationSize");
            settings.TrySetFromEnviron(settings, "ImageWidth");
            settings.TrySetFromEnviron(settings, "ImageHeight");

            if (!settings.TrySetFromEnviron(settings, "RoiX") ||
                !settings.TrySetFromEnviron(settings, "RoiY") ||
                !settings.TrySetFromEnviron(settings, "RoiW") ||
                !settings.TrySetFromEnviron(settings, "RoiH")
            )
            {
                settings.RoiX = 0.0;
                settings.RoiY = 0.0;
                settings.RoiW = 1.0;
                settings.RoiH = 1.0;
            }
            
            logger.Debug("Core settings loaded from environment variables.");
        }
    }
}
