using System;
using System.IO;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using NLog;
using RabbieCam.Utils;
using Swan;

namespace RabbieCam.Capture
{
    /// <summary>
    ///  Camera driver for Raspberry Pi camera module.
    /// </summary>
    public class RPiCamera : ICamera
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Image capture script.
        /// </summary>
        private static readonly string captureCommand = "raspistill";

        /// <summary>
        /// Datetime format for output file name.
        /// </summary>
        private  string dateformat = "yyyy-MM-ddTHHmmssZ";

        /// <summary>
        /// Limit access to 1 thread at a time.
        /// </summary>
        private static readonly SemaphoreSlim resourceLock = 
            new SemaphoreSlim(1, 1);

        /// <summary>
        /// Time (ms) to wait to acquire lock on camera resource.
        /// </summary>
        private static readonly int waitTimeout = 5;

        /// <summary>
        /// Buffer for camera frames.
        /// </summary>
        private Channel<byte[]> channel;

        /// <summary>
        /// Reader for frame buffer.
        /// </summary>
        private ChannelReader<byte[]> reader;

        /// <summary>
        /// Writer for frame buffer.
        /// </summary>
        private ChannelWriter<byte[]> writer;

        /// <summary>
        /// Temporary storage used to build up an image from binary blobs of
        /// image data.
        /// </summary>
        private MemoryStream? tempImage;

        /// <summary>
        /// JPEG marker flag.
        /// </summary>
        private static readonly byte jpegMarker = 0xFF;

        /// <summary>
        /// JPEG image start marker (SOI).
        /// </summary>
        private static readonly byte jpegStartMarker = 0xD8;

        /// <summary>
        /// JPEG image end marker (EOI).
        /// </summary>
        private static readonly byte jpegEndMarker = 0xD9;

        /// <summary>
        /// Is this marker the first start marker in the image?
        /// </summary>
        private bool firstStartMarker = true;

        /// <summary>
        /// Is this marker the last end marker in the image?
        /// </summary>
        private bool lastEndMarker = false;

        /// <summary>
        /// Cancellation source to stop the video stream.
        /// </summary>
        private CancellationTokenSource streamCancellationTokenSource;

        /// <summary>
        /// Ongoing video stream task. Null if not running.
        /// </summary>
        private Task? videoStreamTask;

        /// <summary>
        /// Instantiate RPiCamera class.
        /// </summary>
        public RPiCamera() {}

        /// <summary>
        /// Initialise image stream, resetting parameters for new data.
        /// </summary>
        private void InitialiseStream()
        {
            channel = Channel.CreateBounded<byte[]>(new BoundedChannelOptions(2)
            {
                FullMode = BoundedChannelFullMode.DropOldest,
                SingleReader = true,
                SingleWriter = true
            });
            reader = channel.Reader;
            writer = channel.Writer;
            streamCancellationTokenSource = new CancellationTokenSource();
            firstStartMarker = true;
            lastEndMarker = false;
        }

        /// <summary>
        /// Generate a UTC based file based on current time in directory
        /// specified by IMAGES_PATH environment variable.
        /// </summary>
        public string FilePath(CameraSettings settings)
        {
            var timestamp = DateTime.UtcNow.ToString(dateformat);
            var directory = Environs.ImagesPath;
            var fileName = $"{timestamp}.{settings.Encoding}";

            return Path.Combine(directory, fileName);
        }

        /// <summary>
        /// Async image capture, using current datetime for output file path.
        /// </summary>
        public async Task<string> CaptureToFileAsync()
        {
            var settings = CameraStillSettings.FromEnvirons();
            var filePath = FilePath(settings);
            var cancellationToken = CancellationToken.None;
            await CaptureToFileAsync(settings, cancellationToken);
            return filePath;
        }

        /// <summary>
        /// Async image capture, using current datetime for output file path.
        /// </summary>
        /// <param name="settings">Supplied camera settings</param>
        /// <param name="cancellationToken">Process cancellation token.</param>
        public async Task<string> CaptureToFileAsync(
            CameraStillSettings settings,
            CancellationToken cancellationToken
        )
        {
            var filePath = FilePath(settings);
            await CaptureToFileAsync(
                filePath,
                settings,
                cancellationToken
            );
            return filePath;
        }

        /// <summary>
        /// Async image capture, using current datetime for output file path.
        /// </summary>
        /// <param name="filePath">Path to write image to.</param>
        /// <param name="settings">Supplied camera settings.</param>
        /// <param name="cancellationToken">Process cancellation token.</param>
        public async Task CaptureToFileAsync(
            string filePath,
            CameraStillSettings settings,
            CancellationToken cancellationToken
        )
        {
            var imageData = await CaptureAsync(settings, cancellationToken)
                .ConfigureAwait(false);

            try
            {
                Directory.CreateDirectory(Path.GetDirectoryName(filePath));
            }
            catch (Exception e)
            {
                logger.Error(
                    $"Failed to create image directory: {e.Message}"
                );
                throw new IOException("Failed to create image directory.", e);
            }
            
            try
            {
                // Override the extension with the encoding in settings
                var path = Path.ChangeExtension(filePath, settings.Encoding);
            }
            catch (ArgumentException e)
            {
                logger.Error($"Failed to set file path extension: {e.Message}");
                throw new IOException("Failed to set file path extension.", e);
            }

            try
            {
                await File.WriteAllBytesAsync(filePath, imageData)
                    .ConfigureAwait(false);
            }
            catch (Exception e)
            {
                logger.Error(
                    $"Failed to write image data to file: {e.Message}"
                );
            }
        }

        /// <summary>
        /// Async image capture.
        /// </summary>
        /// <param name="settings">Camera settings.</param>
        /// <param name="cancellationToken">Process cancellation token.</param>
        public async Task<byte[]> CaptureAsync(
            CameraStillSettings settings,
            CancellationToken cancellationToken
        )
        {           
            if (Environs.LaptopEnvironment)
            {
                return await MockCaptureAsync(settings, cancellationToken)
                    .ConfigureAwait(false);
            }
            else
            {
                return await RaspiCaptureAsync(settings, cancellationToken)
                    .ConfigureAwait(false);
            }
        }

        /// <summary>
        /// Capture image using raspberry pi camera in an asynchronous task.
        /// </summary>
        /// <param name="settings">Camera settings.</param>
        /// <param name="cancellationToken">Process cancellation token.</param>
        private async Task<byte[]> RaspiCaptureAsync(
            CameraStillSettings settings,
            CancellationToken cancellationToken
        )
        {
            Acquire();

            try{
                var args = settings.Args("-");

                logger.Debug(
                    $"Capturing image with settings: {settings.ToString()}."
                );

                using var output = new MemoryStream();
                var exitCode = await ProcessRunner.RunProcessAsync(
                    captureCommand,
                    args,
                    null,
                    (data, proc) => output.Write(data, 0, data.Length),
                    null,
                    Definitions.CurrentAnsiEncoding,
                    true,
                    cancellationToken
                ).ConfigureAwait(false);

                if (exitCode == 0)
                {
                    var imageData = output.ToArray();
                    logger.Info($"Captured new image: {imageData.Length} bytes");
                    return imageData;
                }
                else
                {
                    logger.Error(
                        $"Image capture error. Exit code: {exitCode}."
                    );
                    throw new CaptureException($"Image capture error.");
                }
            }
            finally
            {
                Release();
            }
        }

        /// <summary>
        /// Mock image capture in an asynchronous task.
        /// </summary>
        /// <param name="settings">Camera settings.</param>
        /// <param name="cancellationToken">Cancellation token.</param>
        public async Task<byte[]> MockCaptureAsync(
            CameraStillSettings settings,
            CancellationToken cancellationToken
        )
        {
            return await Task.Run(
                () => MockCapture(settings),
                cancellationToken
            ).ConfigureAwait(false);
        }

        /// <summary>
        /// Draw a blank image with a timestamp written on to show the image
        /// capture pipeline is generally working on a computer that doesn't
        /// have a raspberry pi camera.
        /// </summary>
        /// <param name="settings">Camera settings.</param>
        public byte[] MockCapture(CameraSettings settings)
        {
            try
            {
                int width = 640;
                int height = 480;
                Image image = new Bitmap(width, height);
                Graphics drawing = Graphics.FromImage(image);

                // Paint background
                drawing.Clear(Color.Black);

                // Write text
                Brush textBrush = new SolidBrush(Color.White);
                string text = DateTime.UtcNow.ToString(dateformat);
                var font = new Font(
                    new FontFamily("Arial"),
                    16,
                    FontStyle.Regular
                );
                drawing.DrawString(text, font, textBrush, 10, 10);

                drawing.Save();
                
                textBrush.Dispose();
                drawing.Dispose();

                var jpgEncoder = GetEncoder(ImageFormat.Jpeg);
                var encoder = System.Drawing.Imaging.Encoder.Quality;
                var encoderParameters = new EncoderParameters(1);
                EncoderParameter encoderParameter = new EncoderParameter(
                    encoder, 
                    100L
                );
                encoderParameters.Param[0] = encoderParameter;
                using MemoryStream stream = new MemoryStream();
                image.Save(stream, jpgEncoder, encoderParameters);
                return stream.ToArray();
            }
            catch (Exception e)
            {
                logger.Error($"Failed to draw mock image: {e.Message}");
                throw new CaptureException("Failed to draw mock image.", e);
            }
        }

        /// <summary>
        /// Get image encoder for mock image.
        /// </summary>
        private ImageCodecInfo? GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            
            return null;
        }

        /// <summary>
        /// Function that completes with true when there is a frame available, 
        /// and false when no more frames are available on the channel.
        /// </summary>
        public async ValueTask<bool> FrameAvailableAsync(
            CancellationToken cancellationToken)
        {
            return await reader.WaitToReadAsync(cancellationToken)
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Get next image from frame buffer.
        /// </summary>
        /// <param name="cancellationToken"></param>
        public async Task<byte[]> NextBufferImageAsync(
            CancellationToken cancellationToken
        )
        {
            try
            {
                return await reader.ReadAsync(cancellationToken);
            }
            catch (Exception e)
            {
                logger.Error(
                    $"Failed to retrieve image from buffer: {e.Message}"
                );
                return new byte[]{};
            }
        }

        /// <summary>
        /// Find JPEG start (SOI) and end (EOI) markers in binary blob of image
        /// data.
        /// </summary>
        /// <param name="data">
        /// Blob of image data. May not be all image bytes.
        /// </param>
        /// <param name="startIndex">
        /// Intex of first SOI marker byte in image.
        /// </param>
        /// <param name="endIndex">
        /// Index of last EOI marker byte in image.
        /// </param>
        private void FindJpegMarkers(byte[] data, 
                                     out int? startIndex,
                                     out int? endIndex)
        {
            // Iterate with a for loop for speed
            startIndex = null;
            endIndex = null;
            int potentialMarkerIndex = -1;  // This is cheaper than nullable
            int blockLength = data.Length;
            byte b;
            for (int i=0; i < data.Length; i++)
            {
                b = data[i];

                if (b == jpegMarker)
                {
                    potentialMarkerIndex = i;
                }
                else if (b == jpegStartMarker)
                {
                    if (potentialMarkerIndex >= 0 && 
                        i == potentialMarkerIndex + 1)
                    {
                        if (firstStartMarker)
                        {
                            startIndex = potentialMarkerIndex;
                        }
                        firstStartMarker = !firstStartMarker;
                    }
                }
                else if (b == jpegEndMarker)
                {
                    if (potentialMarkerIndex >= 0 &&
                        i == potentialMarkerIndex + 1)
                    {
                        if (lastEndMarker)
                        {
                            endIndex = potentialMarkerIndex;
                        }
                        lastEndMarker = !lastEndMarker;
                    }
                }
            }
        }

        /// <summary>
        /// Try store image data in frame buffer.
        /// </summary>
        /// <param name="imageData">
        /// Part or all of JPEG image binary data.
        /// </param>
        public void BufferImage(byte[] imageData)
        {
            int? startIndex;
            int? endIndex;
            FindJpegMarkers(imageData, out startIndex, out endIndex);

            if (!(endIndex is null))  // Found the end of a JPEG image
            {
                if (!(tempImage is null))  // There is a temp image to complete
                {
                    int fromIndex = 0;
                    // Write bytes from start or startIndex into temp image
                    if (!(startIndex is null) && startIndex < endIndex)
                    {
                        fromIndex = (int)startIndex;
                    }
                    tempImage.Write(
                        imageData, 
                        fromIndex, 
                        (int)endIndex - fromIndex + 1
                    );

                    // Buffer the whole temp image
                    var image = tempImage.ToArray();
                    if (!writer.TryWrite(image))
                    {
                        logger.Warn("Failed to buffer image.");
                    }

                    // Clear the temp image
                    tempImage = null;
                }
            }

            if (!(startIndex is null))  // Found the start of a JPEG image
            {
                // Create a new temp image
                tempImage = new MemoryStream();

                // Copy bytes to end of block or endIndex into temp image
                int fromIndex = (int)startIndex;
                int toIndex = imageData.Length - 1;
                if (!(endIndex is null) && endIndex > startIndex)
                {
                    toIndex = (int)endIndex;
                }
                tempImage.Write(
                    imageData, 
                    fromIndex, 
                    toIndex - fromIndex + 1
                );
            }

            if ((startIndex is null) && (endIndex is null))
            {
                if (!(tempImage is null))
                {
                    // Copy all the data
                    tempImage.Write(
                        imageData,
                        0,
                        imageData.Length
                    );
                }
            }
        }

        /// <summary>
        /// Opens the video stream with the supplied settings. 
        /// </summary>
        /// <param name="settings">Camera settings.</param>
        public void StartStream(CameraStreamSettings settings)
        {
            InitialiseStream();

            try
            {
                videoStreamTask = Task.Run(
                    async ()=>
                    {
                        await StreamWorkerDoWork(settings);
                    },
                    streamCancellationTokenSource.Token
                );
                videoStreamTask.ConfigureAwait(false);
                logger.Debug("Started image stream task.");
            }
            catch (Exception e)
            {
                logger.Error($"Failed to start stream: {e.Message}");
                throw new CaptureException("Failed to start stream.", e);
            }
        }

        /// <summary>
        /// Close the video stream task.
        /// </summary>
        public void StopStream()
        {
            if (!streamCancellationTokenSource.IsCancellationRequested)
            {
                streamCancellationTokenSource.Cancel();
                logger.Debug("Stream token cancelled.");
                videoStreamTask?.Wait();
                logger.Debug("Stream stopped.");
            }
            tempImage = null;
        }

        /// <summary>
        /// Process to run in worker task to retrieve a sequence of JPEG images
        /// from the camera.
        /// </summary>
        /// <param name="settings">Camera settings.</param>
        private async Task StreamWorkerDoWork(CameraStreamSettings settings)
        {
            try
            {
                if (Environs.LaptopEnvironment)
                {
                    MockStream(settings);
                }
                else
                {
                    await RaspiStreamAsync(settings).ConfigureAwait(false);
                }
            }
            catch (Exception e)
            {
                logger.Error($"Stream worker exception: {e.Message}");
            }
            finally
            {
                try
                {
                    writer.TryComplete();
                }
                catch (Exception e)
                {
                    logger.Error(
                        $"Failed to mark channel as complete: {e.Message}"
                    );
                }
                logger.Debug("Stream worker finished.");
            }            
        }

        /// <summary>
        /// Stream images from raspberry pi camera.
        /// </summary>
        /// <param name="settings">Camrea settings.</param>
        private async Task RaspiStreamAsync(CameraStreamSettings settings)
        {
            Acquire();

            try
            {
                var exitCode = await ProcessRunner.RunProcessAsync(
                    captureCommand,
                    settings.Args("-"),
                    null,
                    (data, proc) => BufferImage(data),
                    null,
                    Definitions.CurrentAnsiEncoding,
                    true,
                    streamCancellationTokenSource.Token
                ).ConfigureAwait(false);

                if (exitCode == 0)
                {
                    logger.Info($"{captureCommand} process completed.");
                    return;
                }
                else
                {
                    logger.Error(
                        $"Stream capture error. Exit code: {exitCode}."
                    );
                    throw new CaptureException($"Image capture error.");
                }
            }
            finally
            {
                Release();
            }

        }

        /// <summary>
        /// Stream mock images.
        /// </summary>
        private void MockStream(CameraStreamSettings settings)
        {
            var startTimeSpan = TimeSpan.Zero;
            var periodTimeSpan = TimeSpan.FromSeconds(1);

            logger.Warn("Generating mock image stream.");
            var timer  = new System.Threading.Timer(
                (e) => {
                    var imageData = MockCapture(
                        settings
                    );
                    BufferImage(imageData);
                }, 
                null, 
                startTimeSpan, 
                periodTimeSpan
            );
            streamCancellationTokenSource.Token.WaitHandle.WaitOne();
            timer.Dispose();
            logger.Debug("Stream process complete.");
        }

        /// <summary>
        /// Check if camera is busy. If not, acquire otherwise throw camera busy 
        /// exception.
        /// </summary>
        public void Acquire()
        {
            if (resourceLock.Wait(waitTimeout))
            {
                logger.Debug("Acquired connection to camera.");
            }
            else
            {
                logger.Error("Camera busy.");
                throw new CameraBusyException("Camera busy.");
            }
        }

        /// <summary>
        /// Release camera lock.
        /// </summary>
        public void Release()
        {
            try
            {
                resourceLock.Release();
                logger.Debug("Released camera resource.");
            }
            catch (SemaphoreFullException)
            {
                // This is not a big issue as only one thread can connect to the
                // camera at once.
                logger.Warn("Camera released more times than expected.");
            }
            catch (ObjectDisposedException)
            {
                logger.Error("Semaphore disposed.");
            }
        }
    }
}