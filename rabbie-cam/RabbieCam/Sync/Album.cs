using System;
using Newtonsoft.Json;

namespace RabbieCam.Sync
{
    /// <summary>
    /// Data class for serialising requests to Google Photos.
    /// </summary>
    public class Album
    {
        /// <summary>
        /// Album unique ID.
        /// </summary>
        public string id {get; set;} = "";

        /// <summary>
        /// Album name.
        /// </summary>
        public string title {get; set;} = "";

        /// <summary>
        /// Serialise class to JSON.
        /// </summary>
        /// <returns></returns>
        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        /// <summary>
        /// Instruction to only include album ID in JSON serialisation if it is
        /// not null or empty. 
        /// </summary>
        public bool ShouldSerializeid()
        {
            return !String.IsNullOrEmpty(id);
        }
    }
}