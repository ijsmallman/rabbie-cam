using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Authentication;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using RabbieCam.Utils;

namespace RabbieCam.Sync
{
    public class PhotoSync
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// HTTP request timeout length in seconds.
        /// </summary>
        private int timeout = 100;

        /// <summary>
        /// HTTP client for making web requests to Google Photos REST API.
        /// </summary>
        private static readonly HttpClient client = new HttpClient();

        /// <summary>
        /// Image extensions supported by Google Photos.
        /// </summary>
        /// <value></value>
        private readonly List<string> extensions = new List<string> {
            "bmp", "gif", "heic", "ico", "jpeg", "png", "tiff", "webp"
        };

        /// <summary>
        /// Access and edit permissions for Google Photos.
        /// </summary>
        private static readonly string[] scopes = {
            "https://www.googleapis.com/auth/photoslibrary",
            "https://www.googleapis.com/auth/photoslibrary.sharing"
        };

        /// <summary>
        /// Base URI for Google Photos REST API.
        /// </summary>
        private readonly string baseAddress = 
            "https://photoslibrary.googleapis.com/v1";

        /// <summary>
        /// Number of pages of albums to search for album ID.
        /// </summary>
        private int searchPages = 10;

        private CancellationToken cancellationToken;

        /// <summary>
        /// Instantiate PhotoSync class.
        /// </summary>
        public PhotoSync() : this(CancellationToken.None){}

        /// <summary>
        /// Instantiate PhotoSync class with cancellation token.
        /// </summary>
        public PhotoSync(CancellationToken cancellationToken)
        {
            this.cancellationToken = cancellationToken;
            client.Timeout = TimeSpan.FromSeconds(timeout);
        }

        /// <summary>
        /// Authorise user credentials with Google via OAuth2. 
        /// </summary>
        public async Task<UserCredential> AuthoriseAsync()
        {
            logger.Debug(
                "Authorising from credentials file " +
                $"\"{Environs.CredentialsPath}\"."
            );

            UserCredential credential;
            using (var stream = new FileStream(Environs.CredentialsPath, 
                                               FileMode.Open, 
                                               FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    scopes,
                    Environs.UserName,
                    cancellationToken,
                    new FileDataStore(Environs.SecretsPath, true)
                );
            }

            if (
                credential.Token.IsExpired(Google.Apis.Util.SystemClock.Default)
            )
            {
                logger.Debug("Refreshing authorization token.");
                var refreshResult =
                    await credential.RefreshTokenAsync(cancellationToken);

                if (!refreshResult)
                {
                    logger.Error("Failed to refresh authorization token.");
                    throw new AuthenticationException(
                        "Failed to refresh token."
                    );
                }
            }
            return credential;
        }

        /// <summary>
        /// Get file extension without full stop.
        /// </summary>
        public string GetExtensionNoPeriod(string path)
        {
            return Path.GetExtension(path).Replace(".","").ToLowerInvariant();
        }

        /// <summary>
        /// List images with supported extension in IMAGES_PATH directory.
        /// </summary>
        public IEnumerable<string> ListImages()
        {
            var imgDir = Environs.ImagesPath;
            var imgPaths = Directory
                .EnumerateFiles(
                    imgDir, "*.*", SearchOption.AllDirectories
                )
                .Where(s => extensions.Contains(GetExtensionNoPeriod(s)));
            logger.Debug($"Found {imgPaths.Count()} image(s) in \"{imgDir}\".");
            return imgPaths;
        }

        /// <summary>
        /// Upload images in IMAGES_PATH directory, then delete from device.
        /// </summary>
        public async Task SyncImagesAsync()
        {
            var credential = await AuthoriseAsync();

            foreach (var filePath in ListImages())
            {
                var isSuccess = 
                    await UploadImageAsync(filePath, "", credential);

                if (isSuccess)
                {
                    if(File.Exists(filePath))
                    {
                        try
                        {
                            File.Delete(filePath);
                            logger.Debug(
                                $"Deleted file \"{filePath}\" on device."
                            );
                        }
                        catch (Exception e)
                        {
                            logger.Error(
                                $"Failed to delete file \"{filePath}\" on " +
                                $"device: {e.Message}"
                            );
                        }
                    }
                    else
                    {
                        // Not sure how this might happen
                        logger.Error(
                            $"File \"{filePath}\" not found on device."
                        );
                    }
                }
            }
        }

#nullable enable

        /// <summary>
        /// Authorize and upload image to Google Photos. 
        /// </summary>
        public async Task<bool> UploadImageAsync(
            string filePath,
            string description
        )
        {
            var credential = await AuthoriseAsync();
            return await UploadImageAsync(filePath, description, credential);
        }

        /// <summary>
        /// Upload image to Google Photos using provided authorization. Add
        /// image to album with same name as device.
        /// </summary>
        public async Task<bool> UploadImageAsync(
            string filePath,
            string description,
            UserCredential credential
        )
        {
            try
            {
                var uploadResponse = await UploadBytesAsync(filePath, 
                                                            credential);
                if (!uploadResponse.IsSuccessStatusCode)
                {
                    logger.Error(
                        "Failed to upload file " +
                        $"\"{filePath}\": {uploadResponse.ReasonPhrase}"
                    );
                    return false;
                }

                var fileName = Path.GetFileName(filePath);

                var uploadToken = 
                    await uploadResponse.Content.ReadAsStringAsync();

                var mediaItemResponse = await CreateMediaItemAsync(
                    fileName,
                    description,
                    uploadToken,
                    credential
                );
                if (!mediaItemResponse.IsSuccessStatusCode)
                {
                    logger.Error(
                        "Failed to create MediaItem for file " + 
                        $"\"{filePath}\": {mediaItemResponse.ReasonPhrase}"
                    );
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Error(
                    "Failed to upload file " +
                    $"\"{filePath}\": {e.Message}"
                );
                return false;
            }

            logger.Info($"Uploaded file \"{filePath}\".");
            return true;
        }
#nullable disable

        /// <summary>
        /// Upload image bytes (step 1 of 2).
        /// </summary>
        private async Task<HttpResponseMessage> UploadBytesAsync(
            string filePath,
            UserCredential credential
        )
        {
            string extension = GetExtensionNoPeriod(filePath);
            if(!extensions.Any(extension.Contains))
            {
                logger.Error($"Invalid file extension \"{extension}\".");
                throw new ArgumentException(
                    $"Invalid file extension \"{extension}\"."
                );
            }
            string memeType = $"image/{extension}";

            byte[] imageData = await File.ReadAllBytesAsync(filePath, 
                                                            cancellationToken);
            var content = new ByteArrayContent(imageData);
            content.Headers.ContentType = new MediaTypeHeaderValue(
                "application/octet-stream"
            );

            var request = new HttpRequestMessage(
                HttpMethod.Post, 
                $"{baseAddress}/uploads"
            )
            {
                Content = content
            };
            request.Headers.Authorization = new AuthenticationHeaderValue(
                credential.Token.TokenType,
                credential.Token.AccessToken
            );
            request.Headers.Add(
                "X-Goog-Upload-Content-Type", 
                memeType
            );
            request.Headers.Add(
                "X-Goog-Upload-Protocol", 
                "raw"
            );

            return await client.SendAsync(request, cancellationToken);
        }

#nullable enable
        /// <summary>
        /// Create MediaItem for image (step 2 of 2).
        /// </summary>
        private async Task<HttpResponseMessage> CreateMediaItemAsync(
            string fileName,
            string description,
            string uploadToken,
            UserCredential credential
        )
        {
            var albumTitle = Environs.DeviceName;
            string? albumId = null;
            if (albumTitle != null)
            {
                albumId = await GetAlbumIdAsync(albumTitle, credential);
            }

            var newMediaItemsJson = new NewMediaItems(
                albumId,
                new NewMediaItem(fileName, description, uploadToken)
            ).ToJson();

            var content = new StringContent(newMediaItemsJson);
            content.Headers.ContentType = new MediaTypeHeaderValue(
                "application/json"
            );

            var request = new HttpRequestMessage(
                HttpMethod.Post,
                $"{baseAddress}/mediaItems:batchCreate"
            ) 
            {
                Content = new StringContent(newMediaItemsJson)
            };
            request.Headers.Authorization = new AuthenticationHeaderValue(
                credential.Token.TokenType,
                credential.Token.AccessToken
            );

            return await client.SendAsync(request, cancellationToken); 
        }

        /// <summary>
        /// Find ID for album title.
        /// </summary>
        private async Task<string?> GetAlbumIdAsync(string albumTitle,
                                                    UserCredential credential)
        {
            string? albumId = null;
            string? nextPageToken = null;
            for (int i =0; i < searchPages; i++)
            {
                UriBuilder builder = new UriBuilder($"{baseAddress}/albums");
                if (!String.IsNullOrEmpty(nextPageToken))
                {
                    builder.Query = $"pageToken={nextPageToken}";
                }

                var request = new HttpRequestMessage(HttpMethod.Get, 
                                                     builder.Uri);
                request.Headers.Authorization = new AuthenticationHeaderValue(
                    credential.Token.TokenType,
                    credential.Token.AccessToken
                ); 

                var response = await client.SendAsync(request, 
                                                      cancellationToken);

                if (!response.IsSuccessStatusCode)
                {
                    logger.Error(
                        "Failed to retrieve album list: " +
                        $"{response.ReasonPhrase}"
                    );
                    return albumId;
                }

                var albums = await response.Content.ReadAsStringAsync();

                ParseAlbumsForAlbumId(albums, 
                                      albumTitle, 
                                      out albumId, 
                                      out nextPageToken);

                // Found album ID or no more pages of albums
                if (!String.IsNullOrEmpty(albumId) || 
                    String.IsNullOrEmpty(nextPageToken))
                {
                    break;
                }
            }

            if (String.IsNullOrEmpty(albumId))
            {
                logger.Debug($"Album \"{albumTitle}\" not found.");
                albumId = await CreateAlbum(albumTitle, credential);
            }

            return albumId;
        }

        /// <summary>
        /// Parse album list response and check if it contains album title.
        /// </summary>
        private void ParseAlbumsForAlbumId(string albums,
                                           string albumTitle,
                                           out string? albumId, 
                                           out string? nextPageToken)
        {
            albumId = null;
            nextPageToken = null;
            
            var parsedAlbums = JObject.Parse(albums);

            var results = parsedAlbums["albums"]?.Children().ToList();
            if (results != null)
            {
                foreach (var result in results)
                {
                    var album = result.ToObject<Album>();
                    if (album?.title.Equals(albumTitle) ?? false)
                    {
                        logger.Debug($"Album \"{albumTitle}\" found.");
                        albumId = album.id;
                        return;
                    }
                }
            }

            nextPageToken = parsedAlbums["nextPageToken"]?.ToString();
        }

        /// <summary>
        /// Create album in Google Photos.
        /// </summary>
        private async Task<string?> CreateAlbum(
            string albumTitle,
            UserCredential credential
        )
        {
            var album = new Album(){title=albumTitle};
            var content = new StringContent(
                "{\"album\": " + album.ToJson() + "}"
            );
            content.Headers.ContentType = new MediaTypeHeaderValue(
                "application/json"
            );

            var request = new HttpRequestMessage(HttpMethod.Post,
                                                 $"{baseAddress}/albums"
            )
            {
                Content = content
            };
             request.Headers.Authorization = new AuthenticationHeaderValue(
                credential.Token.TokenType,
                credential.Token.AccessToken
            ); 

            var response = await client.SendAsync(request, cancellationToken);
            
            string? albumId = null;
            if (response.IsSuccessStatusCode)
            {
                logger.Debug($"Created album \"{albumTitle}\".");
                var albumResponse = JsonConvert.DeserializeObject<Album>(
                    await response.Content.ReadAsStringAsync()
                );
                albumId = albumResponse.id;
            }
            else
            {
                logger.Warn(
                    $"Failed to create album \"{albumTitle}\": " +
                    $"{response.ReasonPhrase}"
                );
            }

            return albumId;
        }
    }
#nullable disable
}
