using Newtonsoft.Json;

/// <summary>
/// Data class for Google Photos SimpleMediaItem object
/// </summary>
public class SimpleMediaItem
{
    public string fileName {get; set;}
    public string uploadToken {get; set;}

    public SimpleMediaItem(string fileName, string uploadToken)
    {
        this.fileName = fileName;
        this.uploadToken = uploadToken;
    }
}

/// <summary>
/// Data class for Google Photos NewMediaItem object
/// </summary>
public class NewMediaItem
{
    public string description {get; set;}
    public SimpleMediaItem simpleMediaItem {get; set;}

    public NewMediaItem(string fileName, string description, string uploadToken)
    {
        this.description = description;
        this.simpleMediaItem = new SimpleMediaItem(fileName, uploadToken);
    }
}

/// <summary>
/// Data class for multiple Google Photos NewMediaItem objects
/// </summary>
public class NewMediaItems
{
    public string? albumId {get; set;} = null!;
    public NewMediaItem[] newMediaItems {get; set;}

    public NewMediaItems(string? albumId, NewMediaItem newMediaItem)
    {
        this.albumId = albumId;
        newMediaItems = new NewMediaItem[] { newMediaItem };
    }

    public bool ShouldSerializealbumId()
    {
        // don't serialize albumId if its null
        return (albumId != null);
    }

    public string ToJson()
    {
        return JsonConvert.SerializeObject(this);
    }
}