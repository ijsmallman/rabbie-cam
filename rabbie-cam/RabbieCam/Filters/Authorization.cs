using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using RabbieCam.Utils;

namespace RabbieCam.Filters
{
    /// <summary>
    /// Basic request filter to limit access based on matching a token in 
    /// Authorization header.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class ApiKeyAttribute : Attribute, IAsyncActionFilter
    {
        private const string authorizationHeader = "Authorization";

        public async Task OnActionExecutionAsync(ActionExecutingContext context, 
                                                 ActionExecutionDelegate next)
        {
            string apiKey;
            try
            {
                apiKey = Environs.ApiKey;
            }
            catch (ArgumentNullException)
            {
                await next();
                return;
            }

            var headers = context.HttpContext.Request.Headers;
            if (headers.ContainsKey(authorizationHeader))
            {
                bool isAuthorized = Validate(
                    headers[authorizationHeader],
                    apiKey
                );
                if (!isAuthorized)
                {
                    context.Result = new UnauthorizedResult();
                }
                else
                {
                    await next();
                }
            }
            else
            {
                context.Result = new UnauthorizedResult();
            }   
        }

        public bool Validate(string token, string apiKey)
        {
            return (token.Equals($"Bearer {apiKey}"));
        }
    }
}