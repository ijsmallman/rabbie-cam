﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using NLog;
using RabbieCam.Filters;
using RabbieCam.Capture;
using RabbieCam.Utils;

namespace RabbieCam.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CameraController : ControllerBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private readonly ICamera camera;

        public bool SaveImage{
            get
            {
                bool saveImage = false;
                try
                {
                    saveImage = Environs.SaveApiImages;
                }
                catch (Exception e) when (e is ArgumentNullException || 
                                          e is FormatException)
                {
                    logger.Warn(
                        "Failed to parse save api images environment " +
                        $"varliable. Using default value: {saveImage}."
                    );
                }
                return saveImage;
            }
        }

        public CameraController(ICamera camera)
        {
            this.camera = camera;
        }

        /// <summary>
        /// Capture a still image.
        /// </summary>
        /// <param name="settings">Optional camera settings.</param>
        /// <returns>Captured image</returns>
        /// <response code="401">Unauthorized.</response>
        /// <response code="500">Server error.</response>
        /// <response code="503">Camera currently busy.</response>
        [ApiKey]
        [HttpGet("CaptureImage")]
        public async Task<ActionResult> CaptureImageAsync(
            [FromQuery] CameraStillSettings settings,
            CancellationToken cancellationToken
        )
        {
            try
            {
                logger.Debug("Recieved image capture GET request.");

                byte[] imageData;
                if (SaveImage)
                {
                    string imagePath = await camera.CaptureToFileAsync(
                        settings,
                        cancellationToken
                    );
                    imageData = await System.IO.File.ReadAllBytesAsync(
                        imagePath
                    );
                }
                else
                {
                    imageData = await camera.CaptureAsync(
                        settings,
                        cancellationToken
                    );
                }

                var file = base.File(imageData, $"image/{settings.Encoding}");

                return file;
            }
            catch (CameraBusyException e)
            {
                logger.Error(
                    $"Failed to capture image: {e.Message}"
                );
                // No need to dispose as connection to camera was never opened
                return StatusCode(StatusCodes.Status503ServiceUnavailable);
            }
            catch (Exception e)
            {
                logger.Error(
                    $"Failed to respond with captured image: {e.Message}"
                );
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        /// <summary>
        /// Open a live video stream.
        /// </summary>
        /// <param name="settings">Optional camera settings.</param>
        /// <returns>MJPEG stream</returns>
        /// <response code="401">Unauthorized.</response>
        /// <response code="500">Server error.</response>
        /// <response code="503">Camera currently busy.</response>
        [ApiKey] 
        [HttpGet("LiveStream")]
        public IActionResult LiveStream(
            [FromQuery] CameraStreamSettings settings,
            CancellationToken cancellationToken
        )
        {
            try
            {
                logger.Debug(
                    "Recieved live stream GET request with settings " +
                    $"{settings.ToString()}."
                );

                camera.StartStream(settings); 

                return new MjpegStream(
                    camera.NextBufferImageAsync,
                    camera.FrameAvailableAsync,
                    camera.StopStream
                );
            }
            catch (CameraBusyException e)
            {
                logger.Error(
                    $"Failed to open stream: {e.Message}"
                );
                // No need to dispose as connection to camera was never opened
                return StatusCode(StatusCodes.Status503ServiceUnavailable);
            }
            catch (Exception e)
            {
                // This will not catch exceptions thrown inside 
                // MjpegStream.ExecuteResultAsync
                logger.Error(
                    $"Failed to respond with stream: {e.Message}"
                );
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
